docker-compose (дополнительное задание 3):

docker-compose up -d --build

### API endpoints:
http://localhost/api/client/ - список/добавление клиентов

http://localhost/api/client/[id]/ - просмотр/удаление/изменение клиента

http://localhost/api/mc/ - список/добавление рассылок

http://localhost/api/mc/[id]/ - просмотр/удаление/изменение рассылки
