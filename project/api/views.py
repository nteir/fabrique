from rest_framework import generics
from multicast.models import Client, Multicast, Message
import api.serializers as serializers


class ClientListAPIView(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = serializers.ClientSerializer

class ClientRetrieveAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = serializers.ClientSerializer

class MulticastListAPIView(generics.ListCreateAPIView):
    queryset = Multicast.objects.all()
    serializer_class = serializers.MulticastSerializer

class MulticastRetrieveAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Multicast.objects.all()
    serializer_class = serializers.MulticastSerializer


class MsgListAPIView(generics.ListCreateAPIView):
    queryset = Message.objects.all()
    serializer_class = serializers.TestMsgSerializer
