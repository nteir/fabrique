from django.urls import path
from api import views


urlpatterns = [
    path('client/', views.ClientListAPIView.as_view(), name='clients'),
    path('client/<int:pk>/', views.ClientRetrieveAPIView.as_view(), name='client_detail'),
    path('mc/', views.MulticastListAPIView.as_view(), name='multicasts'),
    path('mc/<int:pk>/', views.MulticastRetrieveAPIView.as_view(), name='multicast_detail'),
    path('msg/', views.MsgListAPIView.as_view(), name='msg_test'),
]
