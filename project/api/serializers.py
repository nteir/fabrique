from rest_framework import serializers
from django.db.models import Count
from multicast.models import Client, Multicast, Message


class ClientSerializer(serializers.ModelSerializer):
    url_detail = serializers.HyperlinkedIdentityField(view_name='client_detail', lookup_field='pk')

    class Meta:
        model = Client
        fields = [
            'id',
            'url_detail',
            'phone',
            'code',
            'tag',
            'timezone',
        ]


class MessageSerializer(serializers.ModelSerializer):
    statistic = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = ['statistic']

    def get_statistic(self, obj):
        return obj.values('status').order_by('status').annotate(count=Count('status'))


class TestMsgSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class MulticastSerializer(serializers.ModelSerializer):
    url_detail = serializers.HyperlinkedIdentityField(view_name='multicast_detail', lookup_field='pk')
    messages = MessageSerializer(source='message_set', read_only=True)

    class Meta:
        model = Multicast
        fields = [
            'id',
            'url_detail',
            'start_time',
            'end_time',
            'messages',
            'message_text',
            'code',
            'tag',
        ]


