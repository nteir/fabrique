from django.db import models
from django.core.validators import RegexValidator
# from datetime import datetime
from django.utils import timezone
import pytz
import requests
import json
from django.db.models.signals import post_save
from django.dispatch import receiver

TIMEZONES = sorted(tuple(zip(pytz.common_timezones, pytz.common_timezones)))
MSG_STATUS = (
    ('not sent', 'not sent'),
    ('OK', 'OK'),
)


# Create your models here.
class Client(models.Model):
    phone = models.CharField(
        max_length=11,
        validators=[
            RegexValidator(
                regex='^7\d{10}',
                message='Phone number format doesn\'t comply',
            ),
        ]
    )
    code = models.IntegerField(verbose_name='Mobile operator code')
    tag = models.CharField(max_length=255, null=True, blank=True, verbose_name='Client tag')
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC', verbose_name='Time zone')

class Multicast(models.Model):
    start_time = models.DateTimeField(default=timezone.now, verbose_name='Start time')
    end_time = models.DateTimeField(null=True, blank=True, verbose_name='End time')
    message_text = models.TextField(verbose_name='Message text')
    code = models.IntegerField(null=True, blank=True, verbose_name='Mobile operator code')
    tag = models.CharField(max_length=255, null=True, blank=True, verbose_name='Client tag')

class Message(models.Model):
    sent_time = models.DateTimeField(auto_now=True, verbose_name='Sent at')
    status = models.CharField(max_length=8, choices=MSG_STATUS, default='not sent', verbose_name='Status')
    multicast = models.ForeignKey(Multicast, on_delete=models.CASCADE, verbose_name='Multicast')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='Client')


@receiver(post_save, sender=Multicast)
def multicast_save_handler(instance, *args, **kwargs):
    multicast = instance
    now = timezone.now()
    if multicast.start_time < now and multicast.end_time > now:
        run_multicast(multicast.id)

def run_multicast(id):
    endpoint = 'https://probe.fbrq.cloud/v1/send/'
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTI3OTE3NDgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Im50ZWlyIn0.gL_u6e0AyHNtD674vtPYN9kVpuufwDaKXTcSgTvpeMo'
    }

    multicast = Multicast.objects.get(id=id)
    qset = Client.objects.all()
    if multicast.code:
        qset = qset.filter(code=multicast.code)
    if multicast.tag:
        qset = qset.filter(tag=multicast.tag)
    message_list = Message.objects.filter(multicast=multicast)

    for client in qset:
        if multicast.end_time <= timezone.now():
            return
        if message_list.filter(client=client).count() > 0:
            msg = message_list.filter(client=client).first()
        else:
            msg = Message()
            msg.status = 'not sent'
            msg.multicast = multicast
            msg.client = client
            msg.save()
        if msg.status == 'not sent' and multicast.end_time > timezone.now():
            url = endpoint + str(msg.id)
            data = {}
            data['id'] = msg.id
            data['phone'] = int(client.phone)
            data['text'] = multicast.message_text
            json_data = json.dumps(data)
            response = requests.post(url=url, data=json_data, headers=headers)
            json_response = response.json()
            if json_response['code'] == 0:
                msg.status = 'OK'
                msg.save()
