from django.apps import AppConfig


class MulticastConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'multicast'
