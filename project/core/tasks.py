from celery import shared_task
from django.utils import timezone
from celery.utils.log import get_task_logger
from multicast.models import Multicast, run_multicast

logger = get_task_logger(__name__)


@shared_task
def check_multicasts():
    logger.info("check_multicasts just ran.")
    qset = Multicast.objects.filter(start_time__lte=timezone.now()).filter(end_time__gte=timezone.now())
    for cast in qset:
        run_multicast(cast.id)
